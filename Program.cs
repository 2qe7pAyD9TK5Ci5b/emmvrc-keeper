﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace emmVRC_keeper
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Fetching...");
            HttpWebResponse httpWebResponse = (HttpWebResponse)
                WebRequest.Create("https://thetrueyoshifan.com/BakaUpdate.php?libdownload")
                    .GetResponse();

            bool reqError = httpWebResponse.StatusCode != HttpStatusCode.OK;
            if (reqError)
            {
                Console.WriteLine("Request failed!");
                return;
            }

            Console.WriteLine("Converting the response...");
            byte[] bytes = Convert.FromBase64String(
                new StreamReader(httpWebResponse.GetResponseStream() ?? throw new InvalidOperationException())
                    .ReadToEnd());
            bool cachedFileExists = File.Exists("emmVRC.dll");
            if (cachedFileExists)
            {
                Console.WriteLine("Reading the cached lib...");
                byte[] second = File.ReadAllBytes("emmVRC.dll");
                bool filesEqual = bytes.SequenceEqual(second);
                if (filesEqual)
                {
                    Console.WriteLine("No changes detected");
                }
                else
                {
                    using (FileStream fileStream = new FileStream("emmVRC.dll", FileMode.Create, FileAccess.Write))
                    {
                        fileStream.Write(bytes, 0, bytes.Length);
                    }

                    Console.WriteLine(BitConverter.ToString(bytes));
                    Console.WriteLine("Update detected. Please create a diff");
                    Console.WriteLine("emmVRC.dll has been updated");
                }
            }
            else
            {
                Console.WriteLine("No cache detected. Can't compare");
                Console.WriteLine("Writing the converted lib...");
                using (FileStream fileStream2 = new FileStream("emmVRC.dll", FileMode.Create, FileAccess.Write))
                {
                    fileStream2.Write(bytes, 0, bytes.Length);
                }
            }

            Console.WriteLine(string.Empty);
            Console.WriteLine("Press any key to stop...");
            Console.ReadKey();
        }
    }
}